instance of PG_ProviderProfileCapabilities
{
    CapabilityID = "@CLASS@";

    ProviderModuleName = "cmpiLMI_{{ PROJECT_NAME }}";

    ProviderName = "@CLASS@";

    RegisteredProfile = 0;

    OtherRegisteredProfile = "OpenLMI-{{ PROJECT_NAME }}";
    OtherProfileOrganization = "OpenLMI";

    ProfileVersion = "@VERSION@";

    RegisteredSubProfiles = {
    };

    ConformingElements = {
       "@CLASS@"
    };
};
