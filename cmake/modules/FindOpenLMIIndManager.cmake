
find_path(OPENLMI_IND_MANAGER_INCLUDE_DIR
    NAMES ind_manager.h
    HINTS $ENV{OPENLMI_IND_MANAGER_INCLUDE_DIR}
    PATH_SUFFIXES include/openlmi
    PATHS /usr /usr/local
)

find_library(OPENLMI_IND_MANAGER_LIBRARY
    NAMES openlmiindmanager
    HINTS $ENV{OPENLMI_IND_MANAGER_LIB_DIR}
    PATH_SUFFIXES lib64 lib
    PATHS /usr /usr/local
)

set(OPENLMI_IND_MANAGER_LIBRARIES ${OPENLMI_IND_MANAGER_LIBRARY})

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(OPENLMI_IND_MANAGER DEFAULT_MSG OPENLMI_IND_MANAGER_LIBRARIES OPENLMI_IND_MANAGER_INCLUDE_DIR)

mark_as_advanced(OPENLMI_IND_MANAGER_INCLUDE_DIR OPENLMI_IND_MANAGER_LIBRARIES)
