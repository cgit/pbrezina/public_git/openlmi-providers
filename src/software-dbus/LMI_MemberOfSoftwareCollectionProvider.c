/*
 * Copyright (C) 2013-2014 Red Hat, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * Authors: Peter Schiffer <pschiffe@redhat.com>
 */

#include <konkret/konkret.h>
#include "LMI_MemberOfSoftwareCollection.h"
#include "sw-utils.h"

static const CMPIBroker* _cb;

static void LMI_MemberOfSoftwareCollectionInitialize(const CMPIContext *ctx)
{
    lmi_init(provider_name, _cb, ctx, provider_config_defaults);
}

static CMPIStatus LMI_MemberOfSoftwareCollectionCleanup(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    CMPIBoolean term)
{
    CMReturn(CMPI_RC_OK);
}

static CMPIStatus enum_instances(const CMPIResult *cr, const char *ns,
        const short names)
{
    GPtrArray *array = NULL;
    SwPackage sw_pkg;
    unsigned i;
    char error_msg[BUFLEN] = "", elem_name[BUFLEN] = "",
            instance_id[BUFLEN] = "";

    create_instance_id(LMI_SystemSoftwareCollection_ClassName, NULL, instance_id,
                    BUFLEN);

    LMI_SystemSoftwareCollectionRef ssc;
    LMI_SystemSoftwareCollectionRef_Init(&ssc, _cb, ns);
    LMI_SystemSoftwareCollectionRef_Set_InstanceID(&ssc, instance_id);

    init_sw_package(&sw_pkg);

    get_pk_packages(0, &array, error_msg, BUFLEN);
    if (!array) {
        goto done;
    }

    for (i = 0; i < array->len; i++) {
        if (create_sw_package_from_pk_pkg(g_ptr_array_index(array, i),
                &sw_pkg) != 0) {
            continue;
        }

        sw_pkg_get_element_name(&sw_pkg, elem_name, BUFLEN);

        create_instance_id(LMI_SoftwareIdentity_ClassName, elem_name, instance_id,
                BUFLEN);

        free_sw_package(&sw_pkg);

        LMI_SoftwareIdentityRef si;
        LMI_SoftwareIdentityRef_Init(&si, _cb, ns);
        LMI_SoftwareIdentityRef_Set_InstanceID(&si, instance_id);

        LMI_MemberOfSoftwareCollection w;
        LMI_MemberOfSoftwareCollection_Init(&w, _cb, ns);
        LMI_MemberOfSoftwareCollection_Set_Collection(&w, &ssc);
        LMI_MemberOfSoftwareCollection_Set_Member(&w, &si);

        if (names) {
            KReturnObjectPath(cr, w);
        } else {
            KReturnInstance(cr, w);
        }
    }

done:
    if (array) {
        g_ptr_array_unref(array);
    }

    if (*error_msg) {
        KReturn2(_cb, ERR_FAILED, "%s", error_msg);
    }

    CMReturn(CMPI_RC_OK);
}

static CMPIStatus LMI_MemberOfSoftwareCollectionEnumInstanceNames(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop)
{
    return enum_instances(cr, KNameSpace(cop), 1);
}

static CMPIStatus LMI_MemberOfSoftwareCollectionEnumInstances(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const char** properties)
{
    return enum_instances(cr, KNameSpace(cop), 0);
}

static CMPIStatus LMI_MemberOfSoftwareCollectionGetInstance(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const char** properties)
{
    PkPackage *pk_pkg = NULL;
    SwPackage sw_pkg;
    char instance_id[BUFLEN] = "";

    init_sw_package(&sw_pkg);

    create_instance_id(LMI_SystemSoftwareCollection_ClassName, NULL, instance_id,
            BUFLEN);

    LMI_MemberOfSoftwareCollection w;
    LMI_MemberOfSoftwareCollection_InitFromObjectPath(&w, _cb, cop);

    if (strcmp(lmi_get_string_property_from_objectpath(w.Collection.value, "InstanceID"),
            instance_id) != 0) {
        CMReturn(CMPI_RC_ERR_NOT_FOUND);
    }

    if (get_sw_pkg_from_sw_identity_op(w.Member.value, &sw_pkg) != 0) {
        CMReturn(CMPI_RC_ERR_NOT_FOUND);
    }

    get_pk_pkg_from_sw_pkg(&sw_pkg, 0, &pk_pkg);
    free_sw_package(&sw_pkg);
    if (!pk_pkg) {
        CMReturn(CMPI_RC_ERR_NOT_FOUND);
    }
    g_object_unref(pk_pkg);

    KReturnInstance(cr, w);
    CMReturn(CMPI_RC_OK);
}

static CMPIStatus LMI_MemberOfSoftwareCollectionCreateInstance(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const CMPIInstance* ci)
{
    CMReturn(CMPI_RC_ERR_NOT_SUPPORTED);
}

static CMPIStatus LMI_MemberOfSoftwareCollectionModifyInstance(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const CMPIInstance* ci,
    const char**properties)
{
    CMReturn(CMPI_RC_ERR_NOT_SUPPORTED);
}

static CMPIStatus LMI_MemberOfSoftwareCollectionDeleteInstance(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop)
{
    CMReturn(CMPI_RC_ERR_NOT_SUPPORTED);
}

static CMPIStatus LMI_MemberOfSoftwareCollectionExecQuery(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const char* lang,
    const char* query)
{
    CMReturn(CMPI_RC_ERR_NOT_SUPPORTED);
}

static CMPIStatus LMI_MemberOfSoftwareCollectionAssociationCleanup(
    CMPIAssociationMI* mi,
    const CMPIContext* cc,
    CMPIBoolean term)
{
    CMReturn(CMPI_RC_OK);
}

static CMPIStatus associators(
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const char* assocClass,
    const char* resultClass,
    const char* role,
    const char* resultRole,
    const char** properties,
    const short names)
{
    CMPIStatus st;
    char error_msg[BUFLEN] = "";

    st = lmi_class_path_is_a(_cb, KNameSpace(cop),
            LMI_MemberOfSoftwareCollection_ClassName, assocClass);
    lmi_return_if_class_check_not_ok(st);

    if (CMClassPathIsA(_cb, cop, LMI_SystemSoftwareCollection_ClassName, &st)) {
        /* got SystemSoftwareCollection - Collection;
         * where listing only associators names is supported */
        if (!names) {
            CMReturn(CMPI_RC_ERR_NOT_SUPPORTED);
        }

        st = lmi_class_path_is_a(_cb, KNameSpace(cop),
                LMI_SoftwareIdentity_ClassName, resultClass);
        lmi_return_if_class_check_not_ok(st);

        if (role && strcmp(role, LMI_COLLECTION) != 0) {
            goto done;
        }
        if (resultRole && strcmp(resultRole, LMI_MEMBER) != 0) {
            goto done;
        }

        enum_sw_identity_instance_names(0, _cb, KNameSpace(cop), cr, error_msg,
                BUFLEN);
    } else if (CMClassPathIsA(_cb, cop, LMI_SoftwareIdentity_ClassName, &st)) {
        /* got SoftwareIdentity - Member */
        char instance_id[BUFLEN] = "";

        st = lmi_class_path_is_a(_cb, KNameSpace(cop),
                LMI_SystemSoftwareCollection_ClassName, resultClass);
        lmi_return_if_class_check_not_ok(st);

        if (role && strcmp(role, LMI_MEMBER) != 0) {
            goto done;
        }
        if (resultRole && strcmp(resultRole, LMI_COLLECTION) != 0) {
            goto done;
        }

        create_instance_id(LMI_SystemSoftwareCollection_ClassName, NULL, instance_id,
                BUFLEN);

        LMI_SystemSoftwareCollectionRef ssc;
        LMI_SystemSoftwareCollectionRef_Init(&ssc, _cb, KNameSpace(cop));
        LMI_SystemSoftwareCollectionRef_Set_InstanceID(&ssc, instance_id);

        if (names) {
            KReturnObjectPath(cr, ssc);
        } else {
            CMPIObjectPath *o = LMI_SystemSoftwareCollectionRef_ToObjectPath(&ssc, &st);
            CMPIInstance *ci = _cb->bft->getInstance(_cb, cc, o, properties, &st);
            CMReturnInstance(cr, ci);
        }
    }

done:
    if (*error_msg) {
        KReturn2(_cb, ERR_FAILED, "%s", error_msg);
    }

    CMReturn(CMPI_RC_OK);
}

static CMPIStatus LMI_MemberOfSoftwareCollectionAssociators(
    CMPIAssociationMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const char* assocClass,
    const char* resultClass,
    const char* role,
    const char* resultRole,
    const char** properties)
{
    return associators(cc, cr, cop, assocClass, resultClass, role,
            resultRole, properties, 0);
}

static CMPIStatus LMI_MemberOfSoftwareCollectionAssociatorNames(
    CMPIAssociationMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const char* assocClass,
    const char* resultClass,
    const char* role,
    const char* resultRole)
{
    return associators(cc, cr, cop, assocClass, resultClass, role,
            resultRole, NULL, 1);
}

static CMPIStatus references(
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const char* assocClass,
    const char* role,
    const short names)
{
    CMPIStatus st;

    st = lmi_class_path_is_a(_cb, KNameSpace(cop),
            LMI_MemberOfSoftwareCollection_ClassName, assocClass);
    lmi_return_if_class_check_not_ok(st);

    if (CMClassPathIsA(_cb, cop, LMI_SystemSoftwareCollection_ClassName, &st)) {
        /* got SystemSoftwareCollection - Collection */
        if (role && strcmp(role, LMI_COLLECTION) != 0) {
            goto done;
        }

        return enum_instances(cr, KNameSpace(cop), names);
    } else if (CMClassPathIsA(_cb, cop, LMI_SoftwareIdentity_ClassName, &st)) {
        /* got SoftwareIdentity - Member */
        char instance_id[BUFLEN] = "";

        if (role && strcmp(role, LMI_MEMBER) != 0) {
            goto done;
        }

        create_instance_id(LMI_SystemSoftwareCollection_ClassName, NULL, instance_id,
                BUFLEN);

        LMI_SystemSoftwareCollectionRef ssc;
        LMI_SystemSoftwareCollectionRef_Init(&ssc, _cb, KNameSpace(cop));
        LMI_SystemSoftwareCollectionRef_Set_InstanceID(&ssc, instance_id);

        LMI_SoftwareIdentityRef si;
        LMI_SoftwareIdentityRef_InitFromObjectPath(&si, _cb, cop);

        LMI_MemberOfSoftwareCollection w;
        LMI_MemberOfSoftwareCollection_Init(&w, _cb, KNameSpace(cop));
        LMI_MemberOfSoftwareCollection_Set_Collection(&w, &ssc);
        LMI_MemberOfSoftwareCollection_Set_Member(&w, &si);

        if (names) {
            KReturnObjectPath(cr, w);
        } else {
            KReturnInstance(cr, w);
        }
    }

done:
    CMReturn(CMPI_RC_OK);
}

static CMPIStatus LMI_MemberOfSoftwareCollectionReferences(
    CMPIAssociationMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const char* assocClass,
    const char* role,
    const char** properties)
{
    return references(cr, cop, assocClass, role, 0);
}

static CMPIStatus LMI_MemberOfSoftwareCollectionReferenceNames(
    CMPIAssociationMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const char* assocClass,
    const char* role)
{
    return references(cr, cop, assocClass, role, 1);
}

CMInstanceMIStub(
    LMI_MemberOfSoftwareCollection,
    LMI_MemberOfSoftwareCollection,
    _cb,
    LMI_MemberOfSoftwareCollectionInitialize(ctx))

CMAssociationMIStub(
    LMI_MemberOfSoftwareCollection,
    LMI_MemberOfSoftwareCollection,
    _cb,
    LMI_MemberOfSoftwareCollectionInitialize(ctx))

KONKRET_REGISTRATION(
    "root/cimv2",
    "LMI_MemberOfSoftwareCollection",
    "LMI_MemberOfSoftwareCollection",
    "instance association")
