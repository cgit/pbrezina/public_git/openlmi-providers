OpenLMI provider documentation
==============================

This documentation describes remote CIM interface of OpenLMI providers.
Here we assume that the reader has some CIM knowledge and is able to:

* Enumerate instances of a CIM class.
* Call a method on a CIM instance.
* Traverse CIM associations between instances.

Please refer to XXX for documentation of OpenLMI client-side tools
and libraries.

Table of Content
----------------

.. toctree::
   :maxdepth: 2

   install
   config
   clones/openlmi-providers/doc/admin/account/index
   clones/openlmi-providers/doc/admin/fan/index
   clones/openlmi-providers/doc/admin/hardware/index
   clones/openlmi-providers/doc/admin/journald/index
   clones/openlmi-providers/doc/admin/logicalfile/index
   clones/openlmi-providers/doc/admin/power/index
   clones/openlmi-providers/doc/admin/realdm/index
   clones/openlmi-providers/doc/admin/service-dbus/index
   clones/openlmi-providers/doc/admin/software/index
   clones/openlmi-networking/doc/admin/index
   clones/openlmi-storage/doc/admin/index

OpenLMI Classes:

.. toctree::
   :maxdepth: 1

   mof/tree
   mof/index

Version
-------

This documentation was automatically generated from:

* openlmi-providers PROVIDERSVER
* openlmi-networking NETWORKINGVER
* openlmi-storage STORAGEVER

.. |time| date::

Generated on |time|.
