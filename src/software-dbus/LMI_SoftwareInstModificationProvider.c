#include <konkret/konkret.h>
#include "LMI_SoftwareInstModification.h"

static const CMPIBroker* _cb = NULL;

static void LMI_SoftwareInstModificationInitialize()
{
}

static CMPIStatus LMI_SoftwareInstModificationIndicationCleanup(
    CMPIIndicationMI* mi,
    const CMPIContext* cc,
    CMPIBoolean term)
{
    CMReturn(CMPI_RC_OK);
}

static CMPIStatus LMI_SoftwareInstModificationAuthorizeFilter(
    CMPIIndicationMI* mi,
    const CMPIContext* cc,
    const CMPISelectExp* se,
    const char* ns,
    const CMPIObjectPath* op,
    const char* user)
{
    CMReturn(CMPI_RC_OK);
}

static CMPIStatus LMI_SoftwareInstModificationMustPoll(
    CMPIIndicationMI* mi,
    const CMPIContext* cc,
    const CMPISelectExp* se,
    const char* ns,
    const CMPIObjectPath* op)
{
    CMReturn(CMPI_RC_ERR_NOT_SUPPORTED);
}

static CMPIStatus LMI_SoftwareInstModificationActivateFilter(
    CMPIIndicationMI* mi,
    const CMPIContext* cc,
    const CMPISelectExp* se,
    const char* ns,
    const CMPIObjectPath* op,
    CMPIBoolean firstActivation)
{
    CMReturn(CMPI_RC_OK);
}

static CMPIStatus LMI_SoftwareInstModificationDeActivateFilter(
    CMPIIndicationMI* mi,
    const CMPIContext* cc,
    const CMPISelectExp* se,
    const char* ns,
    const CMPIObjectPath* op,
    CMPIBoolean lastActivation)
{
    CMReturn(CMPI_RC_OK);
}

static CMPIStatus LMI_SoftwareInstModificationEnableIndications(
    CMPIIndicationMI* mi,
    const CMPIContext* cc)
{
    CMReturn(CMPI_RC_OK);
}

static CMPIStatus LMI_SoftwareInstModificationDisableIndications(
    CMPIIndicationMI* mi,
    const CMPIContext* cc)
{
    CMReturn(CMPI_RC_OK);
}

CMIndicationMIStub(
    LMI_SoftwareInstModification,
    LMI_SoftwareInstModification,
    _cb,
    LMI_SoftwareInstModificationInitialize())

KONKRET_REGISTRATION(
    "root/cimv2",
    "LMI_SoftwareInstModification",
    "LMI_SoftwareInstModification",
    "indication")
