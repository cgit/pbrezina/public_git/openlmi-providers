OpenLMI Power Management Provider Documentation
===============================================

Contents:

.. toctree::
    :maxdepth: 2

    introduction
    concepts
    usage

.. ifconfig:: includeClasses

   OpenLMI Power Management CIM classes:

   .. toctree::
      :maxdepth: 1

      mof/tree
      mof/index
