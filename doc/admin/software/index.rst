.. OpenLMI Software Provider documentation master file, created by
   sphinx-quickstart on Thu Oct  3 14:25:59 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

OpenLMI Software Provider's documentation
=========================================

Contents:

.. toctree::
    :maxdepth: 2

    introduction
    dmtf
    configuration
    usage

.. ifconfig:: includeClasses

   OpenLMI Storage CIM classes:

   .. toctree::
      :maxdepth: 1

      mof/tree
      mof/index


