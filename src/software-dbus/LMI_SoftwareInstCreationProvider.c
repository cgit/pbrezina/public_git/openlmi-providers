#include <konkret/konkret.h>
#include "LMI_SoftwareInstCreation.h"

static const CMPIBroker* _cb = NULL;

static void LMI_SoftwareInstCreationInitialize()
{
}

static CMPIStatus LMI_SoftwareInstCreationIndicationCleanup(
    CMPIIndicationMI* mi,
    const CMPIContext* cc,
    CMPIBoolean term)
{
    CMReturn(CMPI_RC_OK);
}

static CMPIStatus LMI_SoftwareInstCreationAuthorizeFilter(
    CMPIIndicationMI* mi,
    const CMPIContext* cc,
    const CMPISelectExp* se,
    const char* ns,
    const CMPIObjectPath* op,
    const char* user)
{
    CMReturn(CMPI_RC_OK);
}

static CMPIStatus LMI_SoftwareInstCreationMustPoll(
    CMPIIndicationMI* mi,
    const CMPIContext* cc,
    const CMPISelectExp* se,
    const char* ns,
    const CMPIObjectPath* op)
{
    CMReturn(CMPI_RC_ERR_NOT_SUPPORTED);
}

static CMPIStatus LMI_SoftwareInstCreationActivateFilter(
    CMPIIndicationMI* mi,
    const CMPIContext* cc,
    const CMPISelectExp* se,
    const char* ns,
    const CMPIObjectPath* op,
    CMPIBoolean firstActivation)
{
    CMReturn(CMPI_RC_OK);
}

static CMPIStatus LMI_SoftwareInstCreationDeActivateFilter(
    CMPIIndicationMI* mi,
    const CMPIContext* cc,
    const CMPISelectExp* se,
    const char* ns,
    const CMPIObjectPath* op,
    CMPIBoolean lastActivation)
{
    CMReturn(CMPI_RC_OK);
}

static CMPIStatus LMI_SoftwareInstCreationEnableIndications(
    CMPIIndicationMI* mi,
    const CMPIContext* cc)
{
    CMReturn(CMPI_RC_OK);
}

static CMPIStatus LMI_SoftwareInstCreationDisableIndications(
    CMPIIndicationMI* mi,
    const CMPIContext* cc)
{
    CMReturn(CMPI_RC_OK);
}

CMIndicationMIStub(
    LMI_SoftwareInstCreation,
    LMI_SoftwareInstCreation,
    _cb,
    LMI_SoftwareInstCreationInitialize())

KONKRET_REGISTRATION(
    "root/cimv2",
    "LMI_SoftwareInstCreation",
    "LMI_SoftwareInstCreation",
    "indication")
