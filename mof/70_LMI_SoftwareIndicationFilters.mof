/*
 * Copyright (C) 2012-2014 Red Hat, Inc.  All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * Authors: Michal Minar <miminar@redhat.com>
 *
 * These are static CIM_IndicationFilter instances used by software managent
 * providers. They should be installed upon providers installation.
 * They should go to root/interop namespace for sfcb and root/PG_Interop
 * for pegasus.
 */

instance of CIM_IndicationFilter {
    CreationClassName = "CIM_IndicationFilter";
    SystemCreationClassName = "CIM_ComputerSystem";
    SystemName = "HOSTNAME";
    Name = "LMI:LMI_SoftwareInstallationJob:PercentUpdated";
    Query = "SELECT * FROM LMI_SoftwareInstModification WHERE "
            "SourceInstance ISA LMI_SoftwareInstallationJob AND "
            "SourceInstance.CIM_ConcreteJob::PercentComplete <> "
            "PreviousInstance.CIM_ConcreteJob::PercentComplete";
    /* CIM: prefix required for pegasus */
    QueryLanguage = "CIM:CQL";
    Description = "Modification of Percentage Complete for a "
            "Concrete Job.";
    /* required for sfcb - but deprecated */
    SourceNamespace = "root/cimv2";
    SourceNamespaces = {"root/cimv2"};
};

instance of CIM_IndicationFilter {
    CreationClassName = "CIM_IndicationFilter";
    SystemCreationClassName = "CIM_ComputerSystem";
    SystemName = "HOSTNAME";
    Name = "LMI:LMI_SoftwareInstallationJob:Succeeded";
    QueryLanguage = "CIM:CQL";
    Query = "SELECT * FROM LMI_SoftwareInstModification WHERE "
            "SourceInstance ISA LMI_SoftwareInstallationJob AND "
            "SourceInstance.CIM_ConcreteJob::JobState = 17";
    /* This is not supported by sfcb:
     * " = CIM_ConcreteJob.JobState#'Completed'" */
    SourceNamespace = "root/cimv2";
    SourceNamespaces = {"root/cimv2"};
};

instance of CIM_IndicationFilter {
    CreationClassName = "CIM_IndicationFilter";
    SystemCreationClassName = "CIM_ComputerSystem";
    SystemName = "HOSTNAME";
    Name = "LMI:LMI_SoftwareInstallationJob:Failed";
    Query = "SELECT * FROM LMI_SoftwareInstModification WHERE "
            "SourceInstance ISA LMI_SoftwareInstallationJob AND "
            "SourceInstance.CIM_ConcreteJob::JobState = 10";
    /* This is not supported by sfcb:
     * "CIM_ConcreteJob.JobState#'Exception'" */
    QueryLanguage = "CIM:CQL";
    Description = "Modification of Operational Status for a "
            "Concrete Job to 'Complete' and 'OK'.";
    SourceNamespace = "root/cimv2";
    SourceNamespaces = {"root/cimv2"};
};

instance of CIM_IndicationFilter {
    CreationClassName = "CIM_IndicationFilter";
    SystemCreationClassName = "CIM_ComputerSystem";
    SystemName = "HOSTNAME";
    Name = "LMI:LMI_SoftwareInstallationJob:Changed";
    Query = "SELECT * FROM LMI_SoftwareInstModification WHERE "
            "SourceInstance ISA LMI_SoftwareInstallationJob AND "
            "SourceInstance.CIM_ConcreteJob::JobState <> "
            "PreviousInstance.CIM_ConcreteJob::JobState";
    QueryLanguage = "CIM:CQL";
    Description = "Modification of Job State for a ConcreteJob.";
    SourceNamespace = "root/cimv2";
    SourceNamespaces = {"root/cimv2"};
};

instance of CIM_IndicationFilter {
    CreationClassName = "CIM_IndicationFilter";
    SystemCreationClassName = "CIM_ComputerSystem";
    SystemName = "HOSTNAME";
    Name = "LMI:LMI_SoftwareInstallationJob:Created";
    Query = "SELECT * FROM LMI_SoftwareInstCreation WHERE"
            " SourceInstance ISA LMI_SoftwareInstallationJob";
    QueryLanguage = "CIM:CQL";
    Description = "Creation of a ConcreteJob.";
    SourceNamespace = "root/cimv2";
    SourceNamespaces = {"root/cimv2"};
};

instance of CIM_IndicationFilter {
    CreationClassName = "CIM_IndicationFilter";
    SystemCreationClassName = "CIM_ComputerSystem";
    SystemName = "HOSTNAME";
    Name = "LMI:LMI_SoftwareVerificationJob:PercentUpdated";
    Query = "SELECT * FROM LMI_SoftwareInstModification WHERE "
            "SourceInstance ISA LMI_SoftwareVerificationJob AND "
            "SourceInstance.CIM_ConcreteJob::PercentComplete <> "
            "PreviousInstance.CIM_ConcreteJob::PercentComplete";
    /* CIM: prefix required for pegasus */
    QueryLanguage = "CIM:CQL";
    Description = "Modification of Percentage Complete for a "
            "Concrete Job.";
    /* required for sfcb - but deprecated */
    SourceNamespace = "root/cimv2";
    SourceNamespaces = {"root/cimv2"};
};

instance of CIM_IndicationFilter {
    CreationClassName = "CIM_IndicationFilter";
    SystemCreationClassName = "CIM_ComputerSystem";
    SystemName = "HOSTNAME";
    Name = "LMI:LMI_SoftwareVerificationJob:Succeeded";
    QueryLanguage = "CIM:CQL";
    Query = "SELECT * FROM LMI_SoftwareInstModification WHERE "
            "SourceInstance ISA LMI_SoftwareVerificationJob AND "
            "SourceInstance.CIM_ConcreteJob::JobState = 17";
    /* This is not supported by sfcb:
     * " = CIM_ConcreteJob.JobState#'Completed'" */
    SourceNamespace = "root/cimv2";
    SourceNamespaces = {"root/cimv2"};
};

instance of CIM_IndicationFilter {
    CreationClassName = "CIM_IndicationFilter";
    SystemCreationClassName = "CIM_ComputerSystem";
    SystemName = "HOSTNAME";
    Name = "LMI:LMI_SoftwareVerificationJob:Failed";
    Query = "SELECT * FROM LMI_SoftwareInstModification WHERE "
            "SourceInstance ISA LMI_SoftwareVerificationJob AND "
            "SourceInstance.CIM_ConcreteJob::JobState = 10";
    /* This is not supported by sfcb:
     * "CIM_ConcreteJob.JobState#'Exception'" */
    QueryLanguage = "CIM:CQL";
    Description = "Modification of Operational Status for a "
            "Concrete Job to 'Complete' and 'OK'.";
    SourceNamespace = "root/cimv2";
    SourceNamespaces = {"root/cimv2"};
};

instance of CIM_IndicationFilter {
    CreationClassName = "CIM_IndicationFilter";
    SystemCreationClassName = "CIM_ComputerSystem";
    SystemName = "HOSTNAME";
    Name = "LMI:LMI_SoftwareVerificationJob:Changed";
    Query = "SELECT * FROM LMI_SoftwareInstModification WHERE "
            "SourceInstance ISA LMI_SoftwareVerificationJob AND "
            "SourceInstance.CIM_ConcreteJob::JobState <> "
            "PreviousInstance.CIM_ConcreteJob::JobState";
    QueryLanguage = "CIM:CQL";
    Description = "Modification of Job State for a ConcreteJob.";
    SourceNamespace = "root/cimv2";
    SourceNamespaces = {"root/cimv2"};
};

instance of CIM_IndicationFilter {
    CreationClassName = "CIM_IndicationFilter";
    SystemCreationClassName = "CIM_ComputerSystem";
    SystemName = "HOSTNAME";
    Name = "LMI:LMI_SoftwareVerificationJob:Created";
    Query = "SELECT * FROM LMI_SoftwareInstCreation WHERE"
            " SourceInstance ISA LMI_SoftwareVerificationJob";
    QueryLanguage = "CIM:CQL";
    Description = "Creation of a ConcreteJob.";
    SourceNamespace = "root/cimv2";
    SourceNamespaces = {"root/cimv2"};
};
